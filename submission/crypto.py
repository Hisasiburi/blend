from Crypto.PublicKey import RSA
from Crypto.Signature import pkcs1_15
from Crypto.Hash import SHA512
import json

def sign(sk, msg):
    '''
    sign string msg with corresponding secret key (RsaKey object) in pycryptodome library
    return hex value of signature with RSA pkcs1 v1.5
    '''
    h = SHA512.new(msg.encode('utf-8'))
    signature = pkcs1_15.new(sk).sign(h)
    return signature.hex()


def verify(pk, msg, sig):
    '''
    check sign is made for msg using public key PK, string MSG, 
    and byte string SIGN.
    suppose publicExponent is fixed at 0x10001.
    return boolean
    '''
    h = SHA512.new(msg.encode('utf-8'))
    try:
        pkcs1_15.new(pk).verify(h, bytes.fromhex(sig))
        return True
    except(e):
        return False


def load_secret_key(fname):
    '''
    load json information of secret key from fname. 
    This returns RSA key object of pycrytodome library.
    '''
    sk = None
    data = dict()
    with open(fname, 'r') as f:
        data = json.load(f)
    n = int(data['modulus'], 0)
    e = int(data['publicExponent'], 0)
    d = int(data['privateExponent'], 0)
    sk = RSA.construct((n,e,d))
    return sk


def create_secret_key(fname):
    '''
    Create a secret key: [hint] RSA.generate().
    Save the secret key in json to a file named "fname". 
    This returns RSA key object of pycrytodome library.
    '''
    sk = RSA.generate(2048)
    data = dict(
        modulus = hex(sk.n),
        publicExponent = hex(sk.e),
        privateExponent = hex(sk.d)
    )
    with open(fname, 'w') as f:
        json.dump(data, f)
    return sk


def get_hash(msg):
    '''
    return hash hexdigest for string msg with 0x. ex) 0x1a2b...
    '''
    h = SHA512.new(msg.encode('utf-8')).hexdigest()
    return '0x' + h


def get_pk(sk):
    '''
    return pk using modulus of given RsaKey object sk.
    '''
    pk = None
    return sk.publickey()
