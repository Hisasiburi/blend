from ..crypto import get_hash, verify
from ..model import Block, Transaction


class Verifier:
    def verify_transaction(self, transaction: Transaction) -> bool:
        """
        Step 1 : verify transaction
        """
        # check hash
        payload = transaction.get_payload()
        hash_ = get_hash(payload)
        if not hash_ == transaction.hash:
            return False
        return True

    def verify_block(self, block: Block) -> bool:
        """
        Step 2 : verify block
        """
        # check block
        payload = block.get_payload()
        hash_ = get_hash(payload)
        if not hash_ == block.hash:
            return False
        # check hash range
        if not int(block.hash, 0) < 2**512/2**(20 + block.difficulty):
            return False
        return True
