# Blends
## blends/node/crypto.py
```python
def sign(sk, msg)
```
주어진 msg를 sk를 이용하여 signature를 만드는 함수
1. msg를 utf-8로 인코딩
1. 인코딩된 string을 SHA512로해싱
1. pycryptodome의 pkcs1_15 클래스를 이용해 주어진 sk(RSAkey)로 h를 sign
1. sign된 string으로 hexadecimal string으로 리턴
```python
def verify(pk, msg, sig)
```
주어진 msg를 pk를 이용하여 signature와 대응되는 지 확인하는 함수
1. msg를 utf-8로 인코딩
1. SHA512로 해싱
1. pycryptodome의 verify 메소드로 해싱된 string과 signature를 비교
```python
def load_secret_key(fname)
```
fname에 적힌 key 정보를 통해 RSAkey 객체를 만드는 함수
1. fname 오픈
1. modulus, publicExponent, privateExponent를 읽은 뒤,
1. RSA.construct 메소드를 통해 RSAkey 생성
```python
def create_secret_key(fname)
```
fname에 새로 생성한 secret key 정보를 저장하는 함수
1. RSA.generate 메소드로 RSAkey 생성
1. modulus, publicExponent, privateExponent 추출
1. json으로 저장
```python
def get_hash(msg)
```
msg를 hashing 하는 함수
1. msg를 utf-8 인코딩
1. SHA512 hashing
1. hexadecimal string으로 변환
```python
def get_pk(sk)
```
RSAkey로 부터 publickey를 생성하는 함수
1. return sk.publickey()
## blends/node/blockchain/dbmanager.py
```python
class DBManager
```
```python
def search_block(self, block_hash: str) -> Optional[Block]
```
db(blockchain)에서 block_hash를 가진 block를 반환하는 함수
1. sqlalchemy query로 block의 hash가 block_hash를 갖는 row를 가져옴
1. 존재하면 해당 block 리턴
```python
def get_height(self, block_hash: str) -> Optional[int]
```
block_hash를 가진 block의 blockchain상 높이를 반환하는 함수
1. 첫번째 block의 높이는 0
1. block의 순서가 곧 높이이므로 block_hash를 가진 block의 순서를 counting
```python
def get_current(self)
```
가장 마지막으로 등록된 block을 반환하는 함수
1. blockchain에서 block들을 timestamp 기준 descending order로 정렬
1. 정렬된 blockchain에서 첫번째 값 반환
```python
def get_longest(self) -> List[Block]
```
가장 긴 blockchain을 반환하는 함수
1. 이 프레임워크에서는 전체 blockchain과 동치
```python
def search_transaction(self, tx_hash: str) -> Optional[Transaction]
```
tx_hash를 가진 transaction을 반환하는 함수
1. sqlalchemy query로 transaction의 hash가 tx_hash를 갖는 row를 반환
```python
def get_block_balance(self, block_hash: str) -> Dict[str, int]
```
miner마다의 balance 정보를 담은 dictionary를 반환하는 함수
1. 모든 block 탐색
1. 각 block의 balance에 REWARD만큼 할당
1. 모든 block의 transaction을 확인하여
1. sender balance를 amount만큼 줄이고 receiver balance를 amount만큼 증가
1. balance dictionary 반환
## blends/node/blockchain/verifier.py
```python
def verify_transaction(self, transaction: Trnasaction) -> bool
```
transaction이 적절한 format의 attribute를 가지고 있는 지 확인하는 함수
1. 없는 필드 값이 있는 지 확인
1. amount가 REWARD보다 큰 지 확인
1. timestamp가 format이 맞는 지 확인
1. sender와 receiver가 다른지 확인
1. transaction의 필드값이 담긴 payload를 가져와서 hashing한 뒤
1. 원래 가지고 있던 hash값과 비교
```python
def verify_block(self, block: Block) -> bool
```
block이 적절한 attribute를 가지고 있고 주어진 difficulty 범위를 만족하는 지 확인하는 함수
1. 없는 필드 값이 있는 지 확인하고 format이 적절한 지 확인
1. block의 payload를 해싱하여 원래 가지고 있던 hash값과 비교한다.
1. hash값이 2^512/2^(20 + difficulty) 보다 작은 지 확인한다.
## blends/node/blockchain/blockchain.py
```python
def validate_transaction(self, tx: Transaction, block: Block) -> bool
```
주어진 transaction이 blockchain에 들어갈 수 있는 지 확인하는 함수
1. blockchain을 탐색하여 tx와 version, sender, receiver, timestamp가 일치하는 transaction을 찾는다.
1. 찾았다면 duplicate transaction이라는 의미이므로 False를 뱉는다.
1. 못찾았다면 get_block_balance로 block의 balance를 가져온다.
1. tx에 의해 어느 블락의 balance가 0보다 작아지는 상황이 발생한다면 False를 뱉는다.
```python
def validate_block(self, block: Block) -> bool
```
주어진 block이 blockchain에 들어갈 수 있는 지 확인하는 함수
1. block의 parent가 없으면 False
1. block의 parent가 blockchain에 없으면 False
1. 만약 block이 blockchain에 들어있다면
1. 이미 들어있는 block과 주어진 block의 모든 transaction을 비교하여 다른 값이 있다면 False
## blends/node/miner/miner.py
```python
def mine_block(self)
```
주어진 block에서 nonce값을 수정하면서 주어진 difficulty범위를 만족하는 hash값을 찾는 함수
1. THRESHOLD 설정: 2**512/2**(20 + self.block.difficulty)
1. nonce 값을 0부터 block에 대입하여 hash 값 생성
1. hash값이 THRESHOLD보다 작으면 True 리턴하면서 iteration 종료
1. nonce가 99999가 될 때까지 반복
> 이 프레임워크에서는 CPU연산을 하기 때문에 코드 상에서 최적화를 하는 방법이 최선이다. 따라서 nonce를 순차적으로 올리는 방법 대신 50000번까지는 nonce를 랜덤으로 hashing해보고 그 이후부터는 아직 안해본 값으로 순차적으로 nonce를 대입하는 방법을 사용하였다.
```python
def mine_block(self):
  idx = 0
  consumed_nonce = []
  pointer = 0
  THRESHOLD = 2**512/2**(20 + self.block.difficulty)
  while True:
    idx += 1
    if idx % 100000 == 0 and not self.mining.is_set():
      return False
    if idx <= 50000:
      consumed_nonce.append(self.block.nonce)
    payload = self.block.get_payload()
    self.block.hash = get_hash(payload)
    if int(self.block.hash, 0) < THRESHOLD:
      return True
    if idx <= 50000:
      nonce = random.randint(0, 100000)
      while nonce in consumed_nonce:
        nonce = random.randint(0, 100000)
      if idx == 50000:
        consumed_nonce.sort
    else:
      for n in range(pointer, 100000):
        if n in consumed_nonce:
          continue
        nonce = n
        pointer = n+1
        break
    self.block.nonce = nonce
    self.block.timestamp = now()
```
> 그러나 예상과 달리 순차적인 대입보다 더 많은 시간이 걸렸다. 따라서 우선적으로 hash함수에 대한 이해가 선행되어야 한다는 결론을 얻었다.
## blends/node/node.py
```python
def new_requests_received(self, requests: list)
```
network에서 받은 block이나 transaction을 내 노드에 추가하는 함수
1. miner 정지
1. 받은 request가 block인지 transaction인지 확인
1. block이면 이미 내 노드에 존재하는 지 확인하고
1. 없으면 validity확인과 verifying 후 노드에 추가한 뒤 mining할 block 정보를 갱신
1. transaction이면 validity확왼과 verifying 후 현재 mining중인 노드에 기록
1. miner 시작
```python
def new_block_mint(self, block: Block)
```
채굴된 block을 network에 publish하는 함수
1. miner 정지
1. 채굴한 block 정보를 dictionary에 담고 network에 publish
1. miner 시작
```python
def new_transaction_issued(self, tx: Transaction)
```
새로 발생한 transaction을 network에 publish하는 함수
1. transaction 정보를 dictionary에 담고 network에 publish


> cli.py 에 문제가 있어(socket error) Node.run에서 transaction을 직접 넣어 주는 코드를 작성하였다.
```python
self.account_manager = AccountManager('KimHyunsu.json')
receiver_n = 0
sender_n = self.account_manager._secret_key.n
with open('key2.json') as f:
  receiver_n = int(json.loads(f.read())['modulus'], 16)
tx = Transaction.new_transaction('2.0', hex(sender_n), hex(receiver_n), now(), 10)
payload = tx.get_payload()
_hash = crypto.get_hash(payload)
tx.set_hash(_hash)
tx.set_sign(self.account_manager.sign(tx.hash))
self.main_queue.put(tx)
```
> 사용한 key파일은 run.py가 있는 directory의 KimHyunsu.json 파일이다.

