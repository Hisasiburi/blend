import json
import threading
import queue

from .. import DIFFICULTY, BLENDS_VERSION
from .accountmanager import AccountManager
from .blockchain import Blockchain
from .miner import MinerThread
from .model import Block, Transaction
from .networkmanager import NetworkManager
from .parser import Parser
from .util import now

from . import crypto

class Node:
    def __init__(self,
                 broadcast_url: str,
                 key_path: str,
                 db_connection_string: str,
                 difficulty: int = DIFFICULTY):

        self.main_queue = queue.Queue()
        self.network_manager = NetworkManager(broadcast_url, self.main_queue)
        self.account_manager = AccountManager(key_path)
        self.parser = Parser()
        self.blockchain = Blockchain(db_connection_string, difficulty)
        self.mining = None
        self.difficulty = difficulty

        self.height = 0
        self.block = None

    def new_requests_received(self, requests: list):
        # must check the request is block or transaction
        # request is list of requests (from start!)
        # Your code starts here
        self.stop_miner()
        for req in requests:
            print(req)
            if not isinstance(req, dict):
                continue
            ty = req.get('type')
            if ty == 'block':
                if not req.get('payload') or not isinstance(req.get('payload'), str):
                    continue
                payload = json.loads(req.get('payload'))
                block = Block.new_block(
                    version=req.get('version'),
                    parent = payload.get('parent'),
                    timestamp = payload.get('timestamp'),
                    miner = payload.get('miner'),
                    difficulty = payload.get('difficulty')
                )
                block.set_hash(req.get('hash'))
                block.set_nonce(payload.get('nonce'))
                for tx_payload in payload.get('transactions'):
                    block.txs.append(Transaction.new_transaction(
                        tx_payload.get('version'),
                        tx_payload.get('sender'),
                        tx_payload.get('receiver'),
                        tx_payload.get('timestamp'),
                        tx_payload.get('amount')
                    ))
                if not self.blockchain.dbmanager.search_block(block.hash):
                    self.blockchain.append(block)    
                    self.install_new_block()
            elif ty == 'transaction':
                if not req.get('payload') or not isinstance(req.get('payload'), str):
                    continue
                payload = json.loads(req.get('payload'))
                tx = Transaction.new_transaction(
                    req.get('version'),
                    payload.get('sender'),
                    payload.get('receiver'),
                    payload.get('timestamp'),
                    payload.get('amount')
                )
                tx.set_hash(req.get('hash'))
                tx.set_sign(req.get('sign'))
                if not self.block:
                    continue
                if self.blockchain.validate_transaction(tx, self.block) and self.blockchain.verifier.verify_transaction(tx):
                    self.block.txs.append(tx)
            else:
                continue
                    
        # you should have to call self.install_new_block(), self.stop_miner(), self.start_miner(), see below
        # print(requests)
        self.start_miner()

    def new_block_mint(self, block: Block):
        self.stop_miner()
        data = {}
        # Your code starts here
        # you should have to call self.install_new_block(), self.stop_miner(), self.start_miner(), see below
        data = dict(
            type="block",
            hash=block.hash,
            version=block.version,
            payload=block.get_payload()
        )
        print(data)
        self.network_manager.publish(data)
        self.start_miner()

    def new_transaction_issued(self, tx: Transaction):
        data = {}
        # print(tx)
        # Your code starts here
        # you should have to call self.install_new_block(), self.stop_miner(), self.start_miner(), see below
        data = dict(
            type="transaction",
            hash=tx.hash,
            version=tx.version,
            sign=tx.sign,
            payload=tx.get_payload()
        )
        self.network_manager.publish(data)

    def install_new_block(self):
        parent_block = self.blockchain.get_current()
        self.height = self.blockchain.dbmanager.get_height(
            parent_block.hash) + 1
        self.block = Block.new_block(BLENDS_VERSION, parent_block.hash, now(),
                                     self.account_manager.get_public_key(),
                                     self.difficulty)

    def stop_miner(self):
        self.mining.clear()
        self.mining = None
        self.miner = None

    def start_miner(self):
        mining = threading.Event()
        self.mining = mining
        self.mining.set()
        miner = MinerThread(self.block, mining, self.main_queue)
        miner.start()

    def run(self):
        self.install_new_block()
        self.start_miner()
        self.network_manager.start()

        # create transaction instead of cli.py
        self.account_manager = AccountManager('KimHyunsu.json')
        receiver_n = 0
        sender_n = self.account_manager._secret_key.n
        with open('key2.json') as f:
            receiver_n = int(json.loads(f.read())['modulus'], 16)
        tx = Transaction.new_transaction('2.0', hex(sender_n), hex(receiver_n), now(), 10)
        payload = tx.get_payload()
        _hash = crypto.get_hash(payload)
        tx.set_hash(_hash)
        tx.set_sign(self.account_manager.sign(tx.hash))
        self.main_queue.put(tx)

        while True:
            result = self.main_queue.get()
            if type(result) == Block:
                self.new_block_mint(result)
            elif type(result) == Transaction:
                self.new_transaction_issued(result)
            elif type(result) == list:
                self.new_requests_received(result)
