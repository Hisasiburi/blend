from typing import Dict

from ... import DIFFICULTY, GENESIS_HASH, REWARD
from ..model import Block, Transaction
from .dbmanager import DBManager
from .verifier import Verifier


class Blockchain:
    def __init__(self, db_connection_string: str,
                 difficulty: int = DIFFICULTY):

        self.verifier = Verifier()
        self.dbmanager = DBManager(db_connection_string)

    def append(self, block: Block) -> bool:
        if self.verifier.verify_block(block) and self.validate_block(block):
            self.dbmanager.insert_block(block)
            # print("REGISTER to NETWORK")
            return True
        else:
            # print("FAIL to REGISTER to NETWORK")
            return False

    def validate_transaction(self, tx: Transaction, block: Block) -> bool:
        for block_in_db in self.dbmanager.get_longest():
            for block_tx in block_in_db.txs:
                if block_tx.version == tx.version \
                    and block_tx.sender == tx.sender \
                    and block_tx.receiver == tx.receiver \
                    and block_tx.timestamp == tx.timestamp:
                    return False
        balance = self.dbmanager.get_block_balance(block.hash)
        if not balance.get(tx.sender):
            return False
        if balance.get(tx.sender) - tx.amount < 0:
            return False
        return True

    def validate_block(self, block: Block) -> bool:
        """
        check difficulty
        """
        if not block.parent:
            return False
        if not self.dbmanager.search_block(block.parent) and block.parent != '0xdecaf':
            return False
        block_in_db = self.dbmanager.search_block(block.hash)
        if not block_in_db:
            return True
        if not len(block_in_db.txs) == len(block.txs):
            return False
        for tx in zip(block_in_db.txs, block.txs):
            other_tx = tx[0]
            this_tx = tx[1]
            if other_tx.version != this_tx.version \
                or other_tx.sender != this_tx.sender \
                or other_tx.receiver != this_tx.receiver \
                or other_tx.timestamp != this_tx.timestamp:
                return False

        return True

    def get_balance(self) -> Dict[str, int]:
        block = self.dbmanager.get_current()
        balance = self.dbmanager.get_block_balance(block.hash)
        return balance

    def get_current(self):
        return self.dbmanager.get_current()
