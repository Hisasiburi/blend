import threading
import queue
import requests
import random

from ... import PORT
from ..crypto import get_hash
from ..model import Block
from ..parser import Parser
from ..util import now


class Miner:
    def __init__(self,
                 block: Block,
                 mining: threading.Event,
                 main_queue: queue.Queue = None):
        self.parser = Parser()
        self.block = block
        self.mining = mining
        self.main_queue = main_queue

    def mine_block(self):
        idx = 0
        THRESHOLD = 2**512/2**(20 + self.block.difficulty)
        while True:
            idx += 1
            if idx % 100000 == 0 and not self.mining.is_set():
                return False
            """
            Step 1: Your mining code
            """
            payload = self.block.get_payload()
            self.block.hash = get_hash(payload)
            if int(self.block.hash, 0) < THRESHOLD:
                return True
            self.block.nonce += 1
            # self.block.timestamp = now()

    def mine(self):
        if self.mine_block():
            self.new_block_mint()

    def new_block_mint(self):
        if self.main_queue:
            self.main_queue.put(self.block)
