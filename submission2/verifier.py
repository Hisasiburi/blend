from datetime import datetime
from ..crypto import get_hash, verify
from ..model import Block, Transaction

from ... import REWARD


class Verifier:
    def verify_transaction(self, transaction: Transaction) -> bool:
        """
        Step 1 : verify transaction
        """
        if not transaction.hash or not isinstance(transaction.hash, str):
            return False
        if not transaction.version or not isinstance(transaction.version, str):
            return False
        if not transaction.sign or not isinstance(transaction.sign, str):
            return False
        if not transaction.sender or not transaction.receiver:
            return False
        if not transaction.timestamp or not transaction.amount:
            return False
        try:
            # check amount
            if transaction.amount > REWARD:
                return False
            # check timestamp
            if not isinstance(datetime.strptime(transaction.timestamp, '%Y-%m-%dT%H:%M:%S'), datetime):
                return False
            # check sender and receiver
            if transaction.sender == transaction.receiver:
                return False
            # check hash
            payload = transaction.get_payload()
            hash_ = get_hash(payload)
            if not hash_ == transaction.hash:
                return False
        except:
            return False
        return True

    def verify_block(self, block: Block) -> bool:
        """
        Step 2 : verify block
        """
        try:
            if not block.hash or not isinstance(block.hash, str):
                return False
            if not block.version or not isinstance(block.version, str):
                return False
            if not block.parent or not isinstance(block.parent, str):
                return False
            if not block.timestamp or not isinstance(datetime.strptime(block.timestamp, '%Y-%m-%dT%H:%M:%S'), datetime):
                return False
            if not block.miner or not isinstance(block.miner, str) or not isinstance(int(block.miner, 0), int):
                return False
            if block.difficulty is None or block.difficulty < 0:
                return False
            if block.nonce is None or block.nonce < 0:
                return False
        except:
            return False
        # check block
        payload = block.get_payload()
        hash_ = get_hash(payload)
        if not hash_ == block.hash:
            return False
        # check hash range
        if not int(block.hash, 0) < 2**512/2**(20 + block.difficulty):
            return False
        return True
